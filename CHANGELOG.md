# Changelog

## [0.2.1] - 2019-01-16
### Removed
- Dropped support for AAC due to file corruption

## [0.2] - 2019-01-16
### Added
- New layout design
- Small message box at the bottom for printing messages and errors
- Loading message
- New image

### Changed
- Some deep refactoring to simplify the code

### Fixed
- Fixed bug when using multiple songs
- Prevented crash when a music file was corrupted or couldn't be read

### Removed
- Tracklist mode since it had no practical use (and was buggy)
