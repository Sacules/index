Index
======

A tiny and fast music tag editor that aims to please the most demanding folks!

Index provides a very simple and minimalistic TUI that allows for
quick and efficient tagging of multiple records and songs at once.

## Installation
You'll need Go 1.8 at least (usually available by your package manager), and the taglib library, which 
can be installed as follows:

#### Debian
```console
# apt-get update
# apt-get install libtagc0-dev
```

#### Void
```console
# xbps-install -S taglib-devel
```

Afterwards the installation is pretty simple (make sure you have your `GOPATH` set properly):

```console
$ go get -u gitlab.com/Sacules/index
```
This will clone the repo and install Index on `$GOPATH/bin`, and should pull and install all the necessary dependencies.
Use this command each time you want to update Index to the latest version.

## Usage
```console
$ index -h
Usage: index [OPTION]... filename...

Filename has to end in .mp3, .m4a, .ogg, or .flac.

Options:
  -a    auto-tag the given tracks (not implemented yet)
  -d name
        recursively explore the specified directory
  -m    manually tag the given tracks (default)
  -h    print help information
```

By default, Manual tag editing is enabled, so doing

```console
$ index song.mp3
```

will result in the following:

![](/imgs/ManualTag3.png "Manual Tag")

## Features
### Current
* Manual tag editing
* Support for MP3, FLAC, and OGG files.

### Planned
* TUI for Auto Tag
* Fast, parallel searches on multiple providers
* Tabs for editing multiple recordings/tracks
* Approximate/extract artist and record from filename if tags are missing
* Download album art from:
* [ ] Fanartv
* [ ] Musicbrainz
* [ ] iTunes
* [ ] Discogs
* [ ] Album Art Exchange
* [ ] Google
* [ ] Gracenote

* Support for the following metadata providers:
* [X] Musicbrainz
* [ ] Freedb
* [ ] Discogs
* [ ] Last.fm
* [ ] Spotify/Deezer/Other streaming service
* [ ] Bandcamp (hard without a REST API)
* Configuration file to blacklist certain providers / tags / file formats / etc
* Customizable colors
* Show album artwork via w3m-img / ueberzug / pixterm / etc.
* Sort downloaded tags
* Show which tags change when choosing downloaded metadata
* Thorough tag editing with regex

## TODO
* Provide binaries for different platforms
* Oganize the code using a standard approach - divide stuff into small packages?
* Add tests
* Implement writing tags support to [tag](https://github.com/dhowden/tag) and replace go-taglib with it
* Add logs for errors and changes made to the tracks
* Add install instructions for diferent platforms and distributions
