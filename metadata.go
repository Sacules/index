package main

import (
	"time"

	"github.com/Sacules/go-taglib"
)

// TrackMetadata is the basic data all providers should use for each track - this
// is what'd going to be written to the file's metadata, except length
// since it's only used for comparing to the original file's length
type TrackMetadata struct {
	Title       string
	Artist      string
	AlbumArtist string
	Release     string
	Genre       string
	Year        int
	Track       int
	Comment     string
	Length      time.Duration
	Filename    string
	File        *taglib.File

	// Not implemented yet

	// Country     string
	// TrackTotal  int
	// Disc        int
	// DiscTotal   int
	// Publisher   string
	// Artwork []*bytes
}

// RecordMetadata is a collection of TrackMetadata, to be used with
// auto-tagging, to match the tracks with the fetched metadata from
// several providers.
type RecordMetadata struct {
	Format   string
	Position int
	Tracks   []*TrackMetadata
}
