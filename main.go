package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/Sacules/go-taglib"
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

const (
	usage = `Usage: index [OPTION]... filename...

Filename has to end in .mp3, .m4a, .ogg, or .flac.

Options:
  -a    auto-tag the given tracks (not implemented yet)
  -d name
        recursively explore the specified directory
  -m    manually tag the given tracks (default)
  -h	print help information`
)

var (
	// Flags
	dir    string
	manual bool
	auto   bool
	help   bool

	// Channels
	filech = make(chan *taglib.File)
	msgch  = make(chan string)
	errch  = make(chan error)

	// TUI
	app    = tview.NewApplication()
	flex   = tview.NewFlex()
	msgbox = tview.NewTextView()
)

func init() {
	// Flag parsing
	flag.StringVar(&dir, "d", "", "recursively explore the specified directory")
	flag.BoolVar(&manual, "m", true, "manually tag the given tracks")
	flag.BoolVar(&auto, "a", false, "auto-tag the given tracks (not implemented yet)")
	flag.BoolVar(&help, "h", false, "print help information")
	flag.Parse()

	if help {
		fmt.Println(usage)
		os.Exit(0)
	}

	// Check for enough arguments
	if len(os.Args[1:]) < 1 {
		fmt.Println(usage)
		os.Exit(1)
	}

	// Keybindings
	app.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		switch event.Key() {
		case tcell.KeyEscape:
			app.Stop()
		}

		return event
	})

	// Borders
	tview.Borders.HorizontalFocus = tview.BoxDrawingsLightHorizontal
	tview.Borders.VerticalFocus = tview.BoxDrawingsLightVertical
	tview.Borders.TopLeftFocus = tview.BoxDrawingsLightDownAndRight
	tview.Borders.TopRightFocus = tview.BoxDrawingsLightDownAndLeft
	tview.Borders.BottomLeftFocus = tview.BoxDrawingsLightUpAndRight
	tview.Borders.BottomRightFocus = tview.BoxDrawingsLightUpAndLeft

	// Widget layout
	flex.SetDirection(tview.FlexRow)

	// Message box
	msgbox.SetDynamicColors(true)
	msgbox.SetChangedFunc(func() {
		app.Draw()
	})
}

func main() {
	// Parse from directory or input
	if dir != "" {
		go ReadFilesFromDir(dir, filech)
	} else {
		go ReadFilesFromShell(filech)
	}

	// Modes
	if auto {
		fmt.Println("Not implemented yet.")
		os.Exit(1)
	}

	if manual {
		manualtag := NewManualTag()
		go manualtag.Load(filech)

		flex.AddItem(manualtag.GetPrimitive(), 0, 1, true)
	}

	// Add the Message box
	flex.AddItem(msgbox, 1, 1, false)
	go checkMessages()

	// Start the TUI
	app.SetRoot(flex, true).SetFocus(flex)
	if err := app.Run(); err != nil {
		panic(err)
	}
}

func checkMessages() {
	for {
		select {
		case msg := <-msgch:
			msgbox.Clear()
			fmt.Fprintln(msgbox, msg)

		case err := <-errch:
			msgbox.Clear()
			fmt.Fprintf(msgbox, "[red]%s[white]", err)
		}
	}
}
