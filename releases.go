package main

import (
	"sync"

	"github.com/michiwend/gomusicbrainz"
)

// ReleaseMetadata is an album or an EP or a single, etc - just a bunch
// of information about it that each metadata provider returns.
type ReleaseMetadata struct {
	Artist        string
	Release       string
	Year          int
	Country       string
	Type          string
	TrackTotal    int
	Medium        string
	Publisher     string
	Provider      string
	ReleaseID     string
	MusicBrainzID gomusicbrainz.MBID
	// AlbumArtist string
}

// FetchReleases calls the different metadata providers, asking for info on
// the given artist and album (trackTotal if they support it), and returns
// a list of responses.
func FetchReleases(artist, album string, trackTotal int) []*ReleaseMetadata {
	// Main channel to listen for releases
	releasech := make(chan *ReleaseMetadata)
	go callProviders(artist, album, trackTotal, releasech)

	releases := make([]*ReleaseMetadata, 0)
	for release := range releasech {
		releases = append(releases, release)
	}
	return releases
}

// Control the providers are called and the release channel is closed properly
func callProviders(artist, album string, trackTotal int, releasech chan *ReleaseMetadata) {
	var wg sync.WaitGroup

	// Providers
	wg.Add(1)
	go MBSearchRelease(artist, album, trackTotal, 100, releasech, &wg)

	wg.Wait()
	close(releasech)
}
