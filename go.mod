module gitlab.com/sacules/index

go 1.15

require (
	github.com/Sacules/go-taglib v0.0.0-20190117030859-c338c48a3161
	github.com/gdamore/tcell v1.4.0
	github.com/kr/text v0.1.0 // indirect
	github.com/michiwend/golang-pretty v0.0.0-20141116172505-8ac61812ea3f // indirect
	github.com/michiwend/gomusicbrainz v0.0.0-20181012083520-6c07e13dd396
	github.com/rivo/tview v0.0.0-20200915114512-42866ecf6ca6
	golang.org/x/sys v0.0.0-20200916084744-dbad9cb7cb7a // indirect
	golang.org/x/text v0.3.3 // indirect
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
)
